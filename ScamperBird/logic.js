function load_images()
{
batImage=new Image;
batImage.src="assets/Bat.png";
birdImage=new Image;
birdImage.src="assets/Bird.png";
finishLineImage=new Image;
finishLineImage.src="assets/flag.png";
win=new Image;
win.src="assets/win.png";
lose= new Image;
lose.src="assets/losepic.png";
lose2=new Image;
lose2.src="giphy.gif";
}


function init()
{
   
canvas =document.getElementById("canvas");
 H=window.innerHeight;
 W=window.innerWidth;
canvas.width=W;
canvas.height=H;
console.log("got canvas");
pen=canvas.getContext('2d'); //global variable
console.log("pen created"+pen);

    bat5={
        x:420,
        y:480,
        w:120,
        h:80,
        speed:35
        
        };


        bat1={
            x:540,
            y:190,
            w:120,
            h:80,
            speed:35
            
            };

            bat2={
                x:700,
                y:370,
                w:120,
                h:80,
                speed:35
                
                };

                bat3={
                    x:1200,
                    y:100,
                    w:120,
                    h:80,
                    speed:35
                    
                    };
                    bat4={
                        x:940,
                        y:550,
                        w:120,
                        h:80,
                        speed:35
                        
                        }

bat=[bat1,bat2,bat3,bat4,bat5];


bird={
x:15,
y:520,
w:150,
h:150,
moving:false,
score:0,
speed:35
};

//finishLine obj

finishLine={
x:1280,
y:450,
w:250,
h:250,

};




window.addEventListener("keypress",function(){
    console.log("key down");
    bird.moving="true";
});
window.addEventListener("keyup",function(){
    console.log("key up");
    bird.moving="false";
});



}


function isOverlap(rect1,rect2)
{

    if (rect1.x < rect2.x + rect2.w &&
        rect1.x + rect1.w > rect2.x &&
        rect1.y < rect2.y + rect2.h &&
        rect1.h + rect1.y > rect2.y) {
                 console.log("Collision");
           return true;

        }


 }
flag=true;
function draw()
{
   
    pen.clearRect(0,0,W,H);
    pen.drawImage(birdImage,bird.x,bird.y,bird.w,bird.h); //dimn of bird obj
    
    pen.fillStyle="white";
    pen.font="bold 30px serif";
pen.fillText("Score is:"+ bird.score,15,25);
console.log("current score is :"+ bird.score);
    pen.drawImage(finishLineImage,finishLine.x,finishLine.y,finishLine.w,finishLine.h);
    if(flag==false)
    {

        pen.drawImage(win,0,0,1000,800);

    }
    if(isOverlap(bird,finishLine) && bird.x>=W-200)
    {
    
    bird.score+=50;
    console.log("overlapp with finishLine");
    bird.x=15;
        clearInterval(x);
        flag=false;
   draw();
    
    }
for(i=0;i<bat.length;i++)
{
    
if(isOverlap(bat[i],bird))
{
    bird.score-=50;
    console.log("overlapp with bat");
    if(bird.score<0)
    {
        bird.x=15;
        clearInterval(x);
        pen.drawImage(lose,40,30,300,500);//lost
        pen.drawImage(lose2,200,30,300,500);//lost
        console.log("You Lost!!");
    }
}
}

    for(i=0;i<bat.length;i++)
    {

pen.drawImage(batImage,bat[i].x,bat[i].y,bat[i].w,bat[i].h);
//pen.fillStyle="red";
    }
}

function update()
{

    if(bird.moving=="true")
    {
        bird.x+=bird.speed; //bird in -->X direction
        bird.score+=25;
       

}
    
    
 //for boundry setting of bat inside the window height H   
for(i=0;i<bat.length;i++)
{
    bat[i].y+=bat[i].speed;

if(bat[i].y>=H-bat[i].w||bat[i].y<=1 )
{
    
    bat[i].speed*=-1;
}

}

}

function loop()
{

draw();
update();

}

load_images();
init();
var x=setInterval(loop,100);

