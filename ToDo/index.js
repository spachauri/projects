let addForm = document.addfrm; //new Task form
let text = addForm.newTask; //input for new task
let task_score=document.querySelector(".score"); // To get count of tasks done
let count=0; //counter to keep tasks counting
const list_element=document.querySelector("#tasks");

let displayScore = (item) => {
    let str = `You have ${count} pending tasks`;
    task_score.innerText = str;
  };


addForm.addEventListener("submit", (e) => { //once submit event happen in  form
  e.preventDefault();     //reloading prevented
  let item = text.value;  //text is input field
  if (item.length > 0) { //added something ,empty string avoided
    // addItem(item);    //call additem function with new tasks text

    const task_element=document.createElement("div");  //Parent div of any new task item
    task_element.classList.add("tasks");
    
    const task_element_content=document.createElement("div"); // task_element_content is 1 child div of task_element
    task_element_content.classList.add("content");
    
    
  // task_element.appendChild(task_element_content);

    const task_input_element=document.createElement("input");
    task_input_element.classList.add("text");
    task_input_element.type="text";
    task_input_element.value=item;//added
    task_input_element.setAttribute("reaadonly" ,"readonly");


    task_element_content.appendChild(task_input_element);
    task_element.appendChild(task_element_content);
      
      
    const task_action_element=document.createElement("div"); //task_action_element is 2nd child div of task_element
    task_action_element.classList.add("actions");

        const task_action_edit=document.createElement("button");
        task_action_edit.classList.add("edit");
        task_action_edit.innerHTML="edit";

        const task_action_delete=document.createElement("button");
        task_action_delete.classList.add("delete");
        task_action_delete.innerHTML="delete";

        const task_action_done=document.createElement("button");
        task_action_done.classList.add("done");
        task_action_done.innerHTML="done";


        task_action_element.appendChild(task_action_edit);
        task_action_element.appendChild(task_action_delete);
        task_action_element.appendChild(task_action_done);

    task_element.appendChild(task_action_element);
    list_element.appendChild(task_element);

    //list_element(task_element{task_element_content , task_action_element})
      
    count++; //To increment count of task done
    displayScore(task_score);
    console.log("count is"+count);


    console.log("Called AddItem");
    console.log ("You added"+item);


    task_action_edit.addEventListener('click',()=>{
 
      if(task_action_edit.innerText.toLowerCase()=='edit')
      {
        task_input_element.removeAttribute("readonly");
        task_input_element.focus();
        task_action_edit.innerText="save";
        task_input_element.style.backgroundColor='white';
        count++;
        alert("Task is rescheduled to be Done");
        displayScore(task_score);
      }
      else
      {
        task_input_element.setAttribute("readonly","readonly");
        task_action_edit.innerText="edit";
      }
      });

      task_action_delete.addEventListener('click',()=>{
         list_element.removeChild(task_element);
         
         if(count>0)
         {count--;}
         displayScore(task_score);


      });

      task_action_done.addEventListener('click',()=>{

        if(task_action_done.innerText.toLowerCase()=='done'){ //To done task
        task_input_element.style.backgroundColor='rgb(148, 224, 166)';
        if(count>0)
        {count--;}
        displayScore(task_score);
        task_action_done.innerText="undone";
        task_action_done.style.backgroundColor='Red';

        }
        else{//if already done , then on undone
           
          {count++;}
          displayScore(task_score);
          task_action_done.innerText="done";
          task_action_done.style.backgroundColor='rgb(148, 224, 166)';

          task_input_element.style.backgroundColor='white';


        }

     });
    }
    else
    {
      alert("Add some tasks");
    }
    text.value = ""; //to make input blank again for new tasks to be added
});







