const register = document.getElementById('register'); 
const toLogin=document.getElementById('toLogin');

 register.addEventListener('click' , (event)=>{ 
      
     event.preventDefault(); //To prevent default loading on submit
     
     var count=localStorage.getItem('count'); //create a count in local storage
  
     var email = document.getElementById('email'); 
     var firstName = document.getElementById('first'); 
     var lastName = document.getElementById('last'); 
     var userName = document.getElementById('username'); 
     var gender = document.getElementById('gender'); 
     var role = document.getElementById('role'); 
     var password = document.getElementById('password'); 
      var exist = false; 
     for(let i=0;i<localStorage.length;i++) 
     { 
      
     if(localStorage.key(i) != 'count'){ 
  
         var x = localStorage.getItem(localStorage.key(i)); 
         x = JSON.parse(x); 
  
         if( x['email'] == email.value ) 
         { 
             exist=true; 
         } 
          
     } 
     } 
  
     if(exist) 
     { 
         window.alert('This Email address already registered') 
         window.location.replace('register.html') 
         return; 
     } 
  
     if(email.value =='' || firstName.value =='' || lastName.value =='' ||userName.value =='' || gender.value =='' || role.value =='' || password.value =='' ) 
     { 
         window.alert('Filling all fields is mandatory !!! ') 
         return  
     } 
  
     if(password.value.length < 8) 
     { 
         window.alert('Password should be at least of 8 characters') 
         return ;
     } 
  
     const object =  
     { 
     'email' : email.value, 
     'firstname':  firstName.value, 
     'lastname' : lastName.value, 
     'username': userName.value, 
     'gender':  gender.value, 
     'role' : role.value, 
     'password' : password.value 
  
     } 
      
     localStorage.setItem(`user${count}` , JSON.stringify(object)); 
     
  
     count++; 
     localStorage.removeItem('count') 
     localStorage.setItem('count' , `${count}` ) 
     window.alert('Congratulations You are Successfully Registered \n') 
//      window.location.replace('login.html'); 
          //show login

          var regis=document.getElementById('Register');
          regis.style.display='none';

          var log=document.getElementById('Login');
          log.style.display='block';
  
 });

 toLogin.addEventListener('click' ,(e)=>{

    e.preventDefault();
    var regis=document.getElementById('Register');
          regis.style.display='none';

          var log=document.getElementById('Login');
          log.style.display='block';

 });